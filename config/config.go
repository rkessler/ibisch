package config

const (
	CURRENT_PROJECT string = "ibisch"
	MODULE_MAIN     string = "main"
)

type Config struct {
	// PathImage string := "Test"
}

func CurrentPathProject() string {
	return PATH_PROJECTS + CURRENT_PROJECT + "/"
}

func PathFramework() string {
	return CurrentPathProject() + "framework/"
}

func PathServer() string {
	return CurrentPathProject() + "server/"
}

func PathModuleMain() string {
	return CurrentPathProject() + MODULE_MAIN + "/"
}
