/**
 *
 * @author Johannes Leppert
 *
 */

package routes

import (
	. "framework/interfaces"
	. "framework/route"
	. "projects/ibisch/main/modules"
)

func IndexHandler(main MainOnLoad) {
	OnLoad(NewIndex(main.(*Main)))
}
