package main

import (
	"fmt"
	"io"
	"os"
	. "projects/ibisch/config"
	"time"
)

func main() {

	newFormat := time.Now().Local().Format("2006-01-02")
	indexMap := MakeIndexMap(URL_PAGE, newFormat, "weekly", "0.5")
	paths := []string{"/", "impressum/", "team/", "ankunft/"}

	mapCode := MakeMapCode(indexMap, paths)

	WriteFile(mapCode, "sitemaps")

}

func MakeIndexMap(url string, lastMod string, changeFreq string, priority string) map[string]string {

	indexMap := make(map[string]string)
	indexMap["URL"] = url
	indexMap["LASTMOD"] = lastMod
	indexMap["CHANGEFREQ"] = changeFreq
	indexMap["PRIORITY"] = priority

	return indexMap

}

func MakeMapCode(indexMap map[string]string, paths []string) string {

	mapCode := "<?xml version=\u00221.0\u0022  encoding=\u0022UTF-8\u0022?>"
	mapCode += "<urlset xmlns=\u0022http://www.sitemaps.org/schemas/sitemap/0.9″\u0022>"

	for _, path := range paths {

		mapCode += "<url>" +
			"<loc>" + indexMap["URL"] + path + "</loc>" +
			"<lastmod>" + indexMap["LASTMOD"] + "</lastmod>" +
			"<changefreq>" + indexMap["CHANGEFREQ"] + "</changefreq>" +
			"<priority>" + indexMap["PRIORITY"] + "</priority>" +
			"</url>"
	}

	mapCode += "</urlset>"

	return mapCode

}

func WriteFile(MapCode string, fileName string) {

	file, err := os.Create(fmt.Sprintf("%s.xml", PATH_XML+fileName))

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(" Write to file : " + fileName)

	n, err := io.WriteString(file, MapCode)

	if err != nil {
		fmt.Println(n, err)
	}

	file.Close()

}
