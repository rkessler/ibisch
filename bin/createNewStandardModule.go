package main

import (
	"flag"
	. "framework/bins/createNewStandardModule"
)

func main() {

	moduleName := flag.String("name", "", "The moduleName which should be created.")
	flag.Parse()
	standardModuleCreator := NewStandardModuleCreator()
	standardModuleCreator.CreateNewStandardModule(*moduleName)

}
