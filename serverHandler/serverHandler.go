/**
 * @author Johannes Leppert
 */
package serverHandler

import (
	. "framework/regexpHandler"
	. "framework/route"
	"net/http"
	. "projects/ibisch/main/modules"
	. "projects/ibisch/routes"
)

type ServerHandler struct {
	*RegexpHandler
}

func NewServerHandler() *ServerHandler {
	return &ServerHandler{NewRegexpHandler()}
}

func (sh *ServerHandler) StartHandler() {
	sh.AddStaticsCached(MainStaticsURL())
	sh.AddRoute("^\\/$", "", "index", IndexHandler, "main")
}

func (sh *ServerHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	urlFound := false
	currentURL := request.URL.String()

	main := NewMain(sh.RoutesToMap(sh.Routes()), request, writer)
	main.OnLoad()

	for _, route := range sh.Routes() {

		matches := route.Re.FindStringSubmatch(currentURL)

		if matches != nil {

			urlFound = true
			route.Handler(main)
			break

		}

	}

	if urlFound == false {
		OnLoad(NewError(main))
	}

}
