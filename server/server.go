/**
 *
 * @author Johannes Leppert
 *
 */

package main

import (
	"flag"
	. "framework/clog"
	. "framework/gzip"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	. "projects/ibisch/config"
	. "projects/ibisch/serverHandler"
)

var (
	addr = flag.Bool("addr", false, "find open address and print to final-port.txt")
)

func main() {

	flag.Parse()

	if *addr {

		l, err := net.Listen("tcp", "127.0.0.1:0")

		if err != nil {
			log.Fatal(err)
		}

		err = ioutil.WriteFile("final-port.txt", []byte(l.Addr().String()), 0644)

		if err != nil {
			log.Fatal(err)
		}

		s := &http.Server{}
		s.Serve(l)
		return

	}

	STATUS = DEV_STATUS
	DEBUG_ALL = DEV_DEBUG_ALL
	serverHandler := NewServerHandler()
	serverHandler.StartHandler()
	http.Handle("/", Gzip(serverHandler))
	LogFatal("Server Crash: ", http.ListenAndServe(":"+SERVER_PORT, nil))

}
