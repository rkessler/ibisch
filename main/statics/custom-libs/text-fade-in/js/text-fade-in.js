$(document).ready(function() {

    $('div.x-perform a[href^=#]').click(function(e) {
        changeDiv($(this).attr('href'));
    });

    function changeDiv(href) {

        var textDivs = $('#x-perform-text > div');
        hideAll(textDivs);
        $(href).addClass('active');
        $(href + '-').show();

    };

    function hideAll(eles) {

        for (var i = 0, max = eles.length; i < max; i++) {

            var ele = $(eles[i]);

            ele.hide();

            if (ele.hasClass('active')) {
                ele.removeClass('active');
            }

        }

    };

});
