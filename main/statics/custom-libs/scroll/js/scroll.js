$(document).ready(function() {

    var menuSelector = 'nav.main li a[href^=#]';
    var distance = 100;

    $(menuSelector + ', div.x-perform p a[href^=#], a.logo[href^=#], nav.nav-bottom ul li a[href^=#]').click(function(e) {
        var href = $(this).attr('href');
        $('html, body').animate({ scrollTop:$(href).offset().top - distance },'slow');
        e.preventDefault();
    });

    changeColorByScroll(menuSelector);

    $(document).scroll(function() {
        changeColorByScroll(menuSelector);
    });

    function makeDivIds(menu) {

        var ids = [];

        for (var i = 0, max = menu.length; i < max; i++) {
            var id = $(menu[i]).attr('href');
            ids.push(id);
        }

        return ids;

    };

    function changeColor(menu, divIds) {

        var currentPosition = document.documentElement.scrollTop;
        var max = divIds.length;

        for (var i = max - 1; i >= 0; i--) {

            var div = $(divIds[i]);

            if (div.length < 1) {
                continue;
            }

            removeActiveClass(menu);

            if (currentPosition >= div.offset().top - distance - 50) {
                $(menu[i]).addClass('active');
                return
            } else if (currentPosition <= $(divIds[0]).offset().top) {
                // @Attention! Choose the index. It depends on which menupoint it should start
                $(menu[0]).addClass('active');
                return
            } else {
                $(menu[max - 1]).addClass('active');
            }

        }


    };

    function removeActiveClass(eles) {

        for (var i = 0, max = eles.length; i < max; i++) {
            $(eles[i]).removeClass('active');
        }

    };

    function changeColorByScroll(menuSelector) {

        var menu = $(menuSelector);
        var divIds = makeDivIds(menu);
        changeColor(menu, divIds);

    };

});
