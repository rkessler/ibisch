$(document).ready(function() {

    if (getCookie('visited') != 'yes') {

        var logoClass = '.fade-in';

        $(logoClass).addClass('load');
        $(logoClass).click(function() {
            $(this).removeClass('load');
        });

        setCookie('visited', 'yes');

    }

    function getExpiresString() {

        var d = new Date();
        d.setTime(d.getTime() + oneDay());
        return 'expires=' + d.toUTCString();

    };

    function oneDay() {
        return (24*60*60*1000);
    };

    function setCookie(name, value) {
        document.cookie = name + '=' + value + '; ' + getExpiresString();
    };

    function getCookie(name) {
        return getValue(name, document.cookie.split(';'));
    };

    function getValue(cookieName, cookies) {

        var myRegExp = cookieName + '=([^;]+)';

        for (var i = 0, max = cookies.length; i < max; i++) {

            var match = cookies[i].match(myRegExp);
            if (match != null) {

                if (match.length > 0) {

                    if (match[1] == 'yes') {
                        return match[1];
                    }

                }

            }

        }

        return '';

    };

});

