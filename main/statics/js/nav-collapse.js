$(document).ready(function() {

    $('#i-nav').on('click tap', function() {
        $(this).toggleClass('open');
        $('#x-nav').toggleClass('open');
    });

    $('#x-nav > li').on('click tap', function() {
        $('#i-nav').toggleClass('open');
        $('#x-nav').toggleClass('open');
    });

});