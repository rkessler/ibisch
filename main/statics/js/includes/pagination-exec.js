$(document).ready(function() {

    // Only the links without the arrows
    var pagination = new Pagination($('.x-pagination').children().not('.x-arrow').find('a'));
    var urls = pagination.MakePrevAndNextUrls();
    pagination.SwitchByArrows(urls);
    pagination.SwitchByTouch(urls);
    pagination.SetArrow('x-left', urls);
    pagination.SetArrow('x-right', urls);

});