/**
 * @param pagination: This is optional. Because you don't need a pagination list to
 *                    Use this site as a pagination with slide function and stuff
 *
 */
function Pagination(paginationElements) {

    var self = this;
    this.eles = paginationElements;
    this.moduleUrl = makeModuleUrl(window.location.href);
    this.minDistance = 100;

    if (paginationElements == undefined) {
        console.error('You have to put in some Elements. These elements are relevant for the switch');
    }

    this.LinkTo = function(direction, urls) {

        if (direction != 'prevUrl' && direction != 'nextUrl') {
            console.log('direction is not: ' + 'prevUrl' + ' and not: ' + 'nextUrl');
        }

        document.location.href = urls[direction];

    };

    this.MakePrevAndNextUrls = function() {

        var max = this.eles.length;
        var currentIndex = getIndexByUrl(max, this.moduleUrl, this.eles);

        return {
            'prevUrl': this.eles[checkIndex(currentIndex - 1, max)].getAttribute('href'),
            'nextUrl': this.eles[checkIndex(currentIndex + 1, max)].getAttribute('href')
        }

    };

    // Change link of arrow
    this.SetArrow = function(id, urls) {
        $('#' + id + ' a').attr('href', urls[makeDirection(id)]);
    };

    this.SwitchByArrows = function(urls) {

        document.addEventListener('keydown', function(evt) {

            if (evt.keyCode == 37) {
                self.LinkTo('prevUrl', urls)
            } else if (evt.keyCode == 39) {
                self.LinkTo('nextUrl', urls)
            }

        });

    };

    this.SwitchByTouch = function(urls) {

        var startPoint = 0;

        document.addEventListener('touchstart', function(evt) {
            startPoint = evt.clientX;
        });

        document.addEventListener('touchend', function(evt) {

            var distance = startPoint - evt.clientX;

            if (distance > self.minDistance) {
                self.LinkTo('prevUrl', urls);
            } else if (distance < -self.minDistance) {
                self.LinkTo('nextUrl', urls);
            }

        });

    };

    function getIndexByUrl(max, moduleUrl, eles) {

        for (i = 0; i < max; i++) {

            if (moduleUrl == eles[i].getAttribute('href')) {
                return i;
            }

        }

        console.error('The url doesn\'t match any link of these eles!');
        return '';

    };

    // @return the url like /leistungen/modulename/
    function makeModuleUrl(url) {
        var myRegExp = /.+(\/leistungen\/[\w-]+\/)/i;
        return url.match(myRegExp)[1];
    };

    function makeDirection(id) {

        if (id == 'x-left') {
            return 'prevUrl';
        } else if (id == 'x-right') {
            return 'nextUrl';
        } else {
            console.error('id is not correct! current Value: ' + id);
        }

    }

    // To prevent out of index!
    function checkIndex(currentIndex, max) {

        if (currentIndex < 0) {
            return max - 1;
        } else if (currentIndex > max - 1) {
            return 0;
        } else {
            return currentIndex;
        }

    };

}
