$(document).ready(function() {

    var btns = '.x-disable';
    var field = '.x-form-optional';

    $('.x-disable-all').click(function() {

        if ($(btns).hasClass('disabled')) {
            enableCbsAndHideField(btns, field);
        } else {
            disableCbsAndShowField(btns, field);
        }

    });

    // If optional field is empty after submit
    if ($('.x-disable-all').prop('checked')) {
        disableCbsAndShowField(btns, field);
    };

    function disableCbsAndShowField(btns, field) {

        $(btns).addClass('disabled');
        $(btns).attr('disabled', true);
        $(btns).removeAttr('checked');
        $(field).removeClass('display-none');

    };

    function enableCbsAndHideField(btns, field) {

        $(btns).removeClass('disabled');
        $(btns).removeAttr('disabled');
        $(field).addClass('display-none');

    };

});