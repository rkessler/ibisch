package tplHandler

import (
	"bytes"
	"fmt"
	. "framework/templateHandler"
	"html"
	"html/template"
)

type TplHandler struct {
	*TemplateHandler
}

func NewTplHandler(templateHandler *TemplateHandler) *TplHandler {
	return &TplHandler{TemplateHandler: templateHandler}
}

func (th *TplHandler) Render(appName string, tmpl string) {

	tpl := template.Must(template.New("main").ParseFiles(fmt.Sprintf("%stemplates/%s.html", "./main/", appName)))
	th.SetContent("ModuleName", string(tmpl))
	err := tpl.ExecuteTemplate(th.Writer, fmt.Sprintf("%s.html", appName), th)

	if err != nil {
		fmt.Println(err)
	}

}

func (th *TplHandler) Include(path string, tmpl string) template.HTML {

	t := template.Must(template.ParseFiles("./main/" + path + "/templates/" + tmpl + ".html"))
	b := bytes.NewBuffer(nil)

	if err := t.Execute(b, th); err != nil {
		fmt.Println(err)
	}

	return template.HTML(html.UnescapeString(b.String()))

}
