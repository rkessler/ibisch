package modules

type Error struct {
	*Main
	moduleName string
}

func NewError(main *Main) *Error {
	return &Error{Main: main, moduleName: "error"}
}

func (e *Error) OnRequest() {

	e.MetaDataInit(
		"Error - Netzwerktechnik und Webdesign |schommtech.net",
		"Bei schommtech.net werden Webseiten professionell bearbeitet und gepflegt.",
		"noindex, nofollow",
	)

}

func (e *Error) ModuleName() string {
	return e.moduleName
}
