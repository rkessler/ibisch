package modules

import (
	. "framework/functions"
	. "framework/page"
	. "framework/templateHandler"
	"net/http"
	. "projects/ibisch/config"
	. "projects/ibisch/main/structs/tplHandler"
	"time"
)

type Main struct {
	appName string
	*Page
	*TplHandler
	Matches []string
	routes  map[string]string
	request *http.Request
	Writer  http.ResponseWriter
}

func (m *Main) OnLoad() {

	cacheSince := time.Now().Format(http.TimeFormat)
	cacheUntil := time.Now().AddDate(0, 0, 1).Format(http.TimeFormat)
	m.Writer.Header().Add("Last-Modified", cacheSince)
	m.Writer.Header().Add("Expires", cacheUntil)

	m.AddJs("/main/statics/js/jquery_main")
	m.AddCssMin("/main/statics/css/screen_styles", "/main/statics/css/screen_layout_large", "/main/statics/css/screen_layout_medium", "/main/statics/css/screen_layout_small")
	m.AddJsMin("/main/statics/custom-libs/scroll/js/scroll")
	m.AddJsMin("/main/statics/js/nav-collapse")
	m.AddJsMin("/main/statics/js/scroll")
	m.AddJsMin("/main/statics/js/contactTextHide")

	m.SetContent("MenuGroup", "")

}

func NewMain(routes map[string]string, request *http.Request, writer http.ResponseWriter) *Main {
	tplH := NewTemplateHandler([]string{}, []string{}, map[string]string{}, map[string]interface{}{}, routes, writer, MINIFY)
	return &Main{appName: "main", Page: NewPage(), TplHandler: NewTplHandler(tplH), routes: routes, request: request, Writer: writer}
}

func (m *Main) OnRender(mainAppName string, moduleName string) {
	m.Render(mainAppName, moduleName)
}

func (m *Main) Redirect(appName string, query string) {

	var url string

	if MapKeyExists(m.routes, appName) {
		url = "/" + m.routes[appName]
	} else {
		url = appName
	}

	url += query
	http.Redirect(m.Writer, m.Request(), url, 302)

}

func MainStaticsURL() string {
	return "main/statics"
}

func (m *Main) AppName() string {
	return m.appName
}

func (m *Main) Routes() map[string]string {
	return m.routes
}

func (m *Main) SetMatches(x []string) {
	m.Matches = x
}

func (m *Main) Request() *http.Request {
	return m.request
}

func (m *Main) GetPage() *Page {
	return m.Page
}

func (m *Main) GetTpl() *TplHandler {
	return m.TplHandler
}
