package modules

import (
	"strings"
)

type StaticTpl struct {
	*Main
	moduleName  string
	Url         string
	ProjectName string
	regexUrl    string
	ModuleData  map[string]map[string]string
}

func NewStaticTpl(main *Main, name string, url string, regexUrl string, projectName string, moduleData map[string]map[string]string) *StaticTpl {
	return &StaticTpl{Main: main, moduleName: name, Url: url, regexUrl: regexUrl, ProjectName: projectName, ModuleData: moduleData}
}

func (st *StaticTpl) OnRequest() {

	if len(st.ModuleData["MetaData"]) > 2 {

		st.MetaDataInit(
			st.ModuleData["MetaData"]["title"],
			st.ModuleData["MetaData"]["text"],
			st.ModuleData["MetaData"]["robots"],
		)

	}

	for key := range st.ModuleData {
		st.AddOption(key)
	}

}

func (st *StaticTpl) ModuleName() string {
	return st.moduleName
}

func (st *StaticTpl) RegexUrl() string {
	return st.regexUrl
}

/**
*@param key: choose between: JsMin|Js|CssMin|Css|Content
 */
func (st *StaticTpl) AddOption(key string) {

	if len(st.ModuleData[key]) > 0 && key != "MetaData" {

		for firstValue, secondValue := range st.ModuleData[key] {

			switch key {
			case "JsMin":
				st.AddJsMin(firstValue, strings.Split(secondValue, ",")...)
				break
			case "Js":
				st.AddJs(firstValue)
				break
			case "CssMin":
				st.AddCssMin(firstValue, strings.Split(secondValue, ",")...)
				break
			case "Css":
				st.AddCss(firstValue)
				break
			case "Content":
				st.SetContent(firstValue, secondValue)
				break
			}

		}

	}

}
