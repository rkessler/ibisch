package modules

import (
	"fmt"
	. "framework/formHandler"
	"net/url"
)

type Form struct {
	*Main
	*FormHandler
	valid bool
}

func NewForm(main *Main) *Form {
	return &Form{Main: main, FormHandler: NewFormHandler(), valid: true}
}

func (fp *Form) AddErrs() {

	for _, field := range fp.FormFields {

		if !field.Valid() {
			fp.SetInvalid()
			fp.SetContent(fmt.Sprintf("%sErr", field.Name()), field.ErrMsg())
		}

	}

}

func (fp *Form) AddCustomValues() {

	for _, field := range fp.FormFields {
		fp.SetContent(fmt.Sprintf("%sValues", field.Name()), fp.ChooseValue(field.Values()))
	}

}

func (fp *Form) SetContentOfValidation() {
	fp.AddErrs()
	fp.AddCustomValues()
}

/**
 *
 * Choose Value if it should be a string or []string. It depends if it's a text input field or a
 * Checkbox input field
 */
func (fp *Form) ChooseValue(values []string) interface{} {

	if len(values) == 1 {
		return values[0]
	} else {
		return values
	}

}

func (fp *Form) FormValue(name string) []string {
	return fp.Request().Form[name]
}

func (fp *Form) FormValues() url.Values {
	return fp.Request().Form
}

func (fp *Form) SetValid(value bool) {
	fp.valid = value
}

func (fp *Form) Valid() bool {
	return fp.valid
}

func (fp *Form) SetInvalid() {

	if fp.Valid() {
		fp.SetValid(false)
	}

}

func (fp *Form) CheckSuccess() error {

	if fp.Valid() {
		return nil
	} else {
		return fmt.Errorf("There are errors. Should be displayed in the tmpl")
	}

}
