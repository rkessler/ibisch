package modules

type SendMailSuccess struct {
	*Main
	moduleName string
}

func NewSendMailSuccess(main *Main) *SendMailSuccess {
	return &SendMailSuccess{Main: main, moduleName: "sendMailSuccess"}
}

func (sms *SendMailSuccess) OnRequest() {}

func (sms *SendMailSuccess) ModuleName() string {
	return sms.moduleName
}
