package modules

import (
	. "framework/cookie"
	. "framework/emailUser"
	. "framework/formFields"
	. "framework/functions"
	. "framework/smtpHandler"
	. "framework/templateHandler"
	. "projects/ibisch/config"
	"time"
)

type Index struct {
	*Form
	moduleName   string
	emails       []string
	subject      string
	CaptchaFonts string
	checkCopy    bool
	cookie       *Cookie
}

func NewIndex(main *Main) *Index {
	return &Index{Form: NewForm(main), moduleName: "index"}
}

func (c *Index) Init() {

	c.AddField(NewTextField("companyName", true, "Bitte targen Sie ihren Firmennamen ein!", 100, nil))
	c.AddField(NewTextField("departmentName", false, "Sie müssen etwas eintragen", 100, nil))
	c.AddField(NewTextField("lastName", true, "Bitte targen Sie ihren Nachnamen ein!", 100, nil))
	c.AddField(NewTextField("firstName", true, "Bitte targen Sie ihren Vornamen ein!", 100, nil))
	c.AddField(NewEmailField("customerEmail", true, "Bitte targen Sie eine gültige E-Mail-Adresse ein!"))
	c.AddField(NewTextField("street", false, "Sie müssen etwas eintragen", 100, nil))
	c.AddField(NewTextField("postCode", false, "Sie müssen etwas eintragen", 100, nil))
	c.AddField(NewTextField("city", false, "Sie müssen etwas eintragen", 100, nil))
	c.AddField(NewTextField("telephone", true, "Bitte targen Sie Ihre Telefonnummer ein!", 100, nil))
	c.AddField(NewTextField("fax", false, "Sie müssen etwas eintragen", 100, nil))
	c.AddField(NewTextField("customerMessage", true, "Bitte targen Sie eine Nachricht ein!", 1000, nil))

}

func (c *Index) OnRequest() {

	c.AddJsMin("/main/statics/custom-libs/logo-fade-in/js/logo-fade-in")
	c.AddCssMin("/main/statics/custom-libs/logo-fade-in/css/logo-fade-in")

	c.MetaDataInit(
		"Professionelles Personalmanagement - ibisch-gmbh.de",
		"Unsere Dienstleistungen sind vielfältig im Bereich des Personalmanagements. Wir suchen nicht nur kompetente Mitarbeiter, sondern übernehmen auch andere Aufgaben in diesem Zusammenhang.",
		"index, follow",
	)

}

func (c *Index) OnSubmit() error {

	c.Validation(c.Request())
	c.SetContentOfValidation()
	return c.CheckSuccess()

}

func (c *Index) OnSuccess() {
	c.SendMail([]string{EMAIL_CONTACT}, "Eine Anfrage wurde versendet")
	c.AddJs("/main/statics/js/sentMail")
}

func (c *Index) ModuleName() string {
	return c.moduleName
}

func (c *Index) SendMail(emails []string, subject string) {

	datetime := time.Now().Format("02.01.2006")
	timestamp := time.Now().Format("03:04")

	msg := HtmlLetter(c.Field("customerMessage").Value())
	smtpTmplH := NewSmtpTmplDataHandler(c.GetTpl().TemplateHandler, EMAIL_USER, emails, subject, msg)
	smtpTmplH.SetContent("User", c.Field("firstName").Value()+" "+c.Field("lastName").Value())
	smtpTmplH.SetContent("Time", datetime)
	smtpTmplH.SetContent("Tstamp", timestamp)
	smtpTmplH.SetContent("Subject", subject)
	smtpTmplH.SetContent("companyName", c.Field("companyName").Value())
	smtpTmplH.SetContent("SenderEmail", c.Field("customerEmail").Value())
	smtpTmplH.SetContent("departmentName", c.Field("departmentName").Value())
	smtpTmplH.SetContent("lastName", c.Field("lastName").Value())
	smtpTmplH.SetContent("firstName", c.Field("firstName").Value())
	smtpTmplH.SetContent("street", c.Field("street").Value())
	smtpTmplH.SetContent("postCode", c.Field("postCode").Value())
	smtpTmplH.SetContent("telephone", c.Field("telephone").Value())
	smtpTmplH.SetContent("fax", c.Field("fax").Value())
	smtpTmplH.SetContent("customerMessage", c.Field("customerMessage").Value())

	body := smtpTmplH.ParseHtmlTmpl("contactEmail")
	smtpH := NewSmtpHandler(NewEmailUser(EMAIL_USER, EMAIL_PW, EMAIL_SMTP, EMAIL_PORT))
	smtpH.SendMail(body, emails)

}
